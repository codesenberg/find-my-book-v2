# Find a Book Web App

This app allows you to search for a book via Google's Book Api (https://developers.google.com/apis-explorer/#p/books/v1/).

This app is based off the react-webpack-babel-sass-boilerplate by Marc Nuri (https://github.com/marcnuri-demo/react-webpack-babel-sass-boilerplate#readme) and is built on top of React, Babel, Webpack, Sass with Hot Module Replacement. It also contains eslint (eslint-config-airbnb) and sass lint.

Please view the original boilerplate code for information on how this app was configured.

To install and run:

```
git clone https://codesenberg@bitbucket.org/codesenberg/find-my-book-v2.git
cd find-my-book-v2
npm i
npm start
// app served at: http://localhost:9000
```

To run lint test:

```
npm run test
```

To build for production:

```
npm run build
```
