import React from 'react';
import { Route } from 'react-router-dom';
import { HomePage, SearchResultsPage } from './pages';

function BookPage() {
  return <div>Book Info</div>;
}

function AppRouter() {
  return (
    <div>
      <Route path="/" exact component={HomePage} />
      <Route path="/search-results" component={SearchResultsPage} />
      <Route path="/book" component={BookPage} />
    </div>
  );
}

export default AppRouter;
