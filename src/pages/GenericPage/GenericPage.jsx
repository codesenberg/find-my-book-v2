import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './GenericPage.scss';

class GenericPage extends Component {
  render() {
    const { message, imageSrc, imageAltText } = this.props;
    return (
      <div className="cp-generic-page">
        <p className="message">{message}</p>
        <div className="image">
          <img src={imageSrc} alt={imageAltText} />
        </div>
      </div>
    );
  }
}

GenericPage.propTypes = {
  message: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
  imageSrc: PropTypes.string.isRequired,
  imageAltText: PropTypes.string.isRequired
};

export default GenericPage;
