import React, { Component } from 'react';
import { GenericPage } from '../GenericPage';
import notFoundImage from '../../globals/images/empty-shelf.png';

class NotFoundPage extends Component {
  render() {
    return (
      <div className="cp-not-found-page">
        <GenericPage
          message={(
            <span>
              Sorry no items were found in your search.
              <br />
              Please search again.
            </span>
          )}
          imageSrc={notFoundImage}
          imageAltText="not found"
        />
      </div>
    );
  }
}

export default NotFoundPage;
