import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import queryString from 'query-string';
import ReactRouterPropTypes from 'react-router-prop-types';
import { ResultItem, SearchBar, Pagination } from '../../components';
import { getBooks } from '../../api';
import { NotFoundPage } from '../NotFoundPage';
import searchingGif from '../../globals/images/searching.gif';

import './SearchResultsPage.scss';

function renderSearchGif() {
  return (
    <div className="searching-gif-holder">
      <img src={searchingGif} alt="searching" />
    </div>
  );
}

class SearchResultsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchResults: [],
      totalItems: 0,
      lastSearchedQuery: '',
      lastSelectedPage: 1,
      isSearching: true
    };
    this.handlePageClick = this.handlePageClick.bind(this);
  }

  componentDidMount() {
    this.fetchBooks();
  }

  componentDidUpdate() {
    const { query, page } = queryString.parse(this.props.location.search);

    /*
        this update detects when the route url query has been changed
        the api request will only be made if:
        -the current search query is not the same as the old query
        -if the current selected page is not the same as the last selected page
        -if a current search request is not in progress
    */
    if ((this.state.lastSearchedQuery !== query || this.state.lastSelectedPage !== page)
      && !this.state.isSearching) {
      this.fetchBooks();
    }
  }

  // calculates the page total for the pagination
  getPageTotal() {
    const totalItems = this.state.totalItems > 100 ? 100 : this.state.totalItems;
    return Math.ceil(totalItems / 10);
  }

  // handles making api request to get search results of books then updates the state
  fetchBooks() {
    const { query, page } = queryString.parse(this.props.location.search);
    if (query) {
      this.setState({ isSearching: true });
      getBooks(query, page || 1).then((results) => {
        this.setState({
          searchResults: results.items,
          totalItems: results.totalItems,
          lastSearchedQuery: query,
          lastSelectedPage: page,
          isSearching: false
        });
      });
    }
  }

  // callback for pagination control to update url query with new selected page
  handlePageClick(currentPage) {
    this.props.history.push({ pathname: '/search-results', search: `?query=${this.state.lastSearchedQuery}&page=${currentPage}` });
  }

  renderItems() {
    return this.state.searchResults.map(result => (
      <div key={`${result.id}`} className="resultItem">
        <ResultItem
          title={result.title}
          authors={result.authors}
          description={result.description}
          averageRating={result.averageRating}
          jacketCover={result.jacketCover}
        />
      </div>
    ));
  }

  renderResultTotal() {
    if (this.state.totalItems !== undefined) {
      const totalItems = this.state.totalItems === 1 ? 'item' : 'items';

      return this.state.totalItems > 100
        ? <span>Top 100 items returned</span>
        : (
          <span>
            {totalItems}
            {' '}
            items returned
          </span>
        );
    }


    return null;
  }

  renderSearchResults() {
    if (this.state.searchResults.length === 0) {
      // if no items are returned show not found page
      return <NotFoundPage />;
    }

    return (
      <div>
        <h1>Search Results</h1>
        <span>{this.renderResultTotal()}</span>
        {this.renderItems()}
        <Pagination
          currentPage={Number(this.state.lastSelectedPage || 1)}
          pageTotal={this.getPageTotal()}
          handlePageClick={this.handlePageClick}
        />
      </div>
    );
  }

  render() {
    return (
      <div className="cp-search-results-page page">
        <SearchBar />

        {this.state.isSearching
          ? renderSearchGif()
          : this.renderSearchResults()}
      </div>
    );
  }
}

const RouterPropTypes = {
  history: ReactRouterPropTypes.history.isRequired
};

SearchResultsPage.propTypes = {
  ...RouterPropTypes
};

export default withRouter(SearchResultsPage);
