import React, { Component } from 'react';
import { GenericPage } from '../GenericPage';
import { SearchBar } from '../../components';

import welcomeImage from '../../globals/images/bookshelf.png';

class HomePage extends Component {
  render() {
    return (
      <div className="cp-home-page">
        <SearchBar />
        <GenericPage
          message={(
            <span>
              Welcome to Find My Book App.
              <br />
              Begin by searching for a book!
            </span>
          )}
          imageSrc={welcomeImage}
          imageAltText="bookshelf"
        />
      </div>
    );
  }
}

export default HomePage;
