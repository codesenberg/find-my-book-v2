import { apiFetch } from './Api';

// handles return list of books which contains metadata matching the search query
export function getBooks(query, page) {
  const url = 'https://www.googleapis.com/books/v1/volumes';

  return apiFetch(`${url}?q=${query}&startIndex=${page}`).then((result) => {
    if (result.items) {
      const items = result.items.map((item, index) => ({
        id: `${item.id}-${index}`, // index added to id to ensure it's unique
        title: item.volumeInfo.title,
        authors: item.volumeInfo.authors,
        averageRating: item.volumeInfo.averageRating,
        description: item.volumeInfo.description,
        jacketCover: item.volumeInfo.imageLinks && item.volumeInfo.imageLinks.thumbnail,
        pageCount: item.volumeInfo.pageCount,
        publishedDate: item.volumeInfo.publishedDate,
        publisher: item.volumeInfo.publisher,
        price: item.saleInfo.retailPrice && item.saleInfo.retailPrice.amount
      }));

      return {
        items,
        totalItems: result.totalItems
      };
    }
    return {
      items: [],
      totalItems: result.totalItems
    };
  });
}
