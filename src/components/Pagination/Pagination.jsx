import React, { Component } from 'react';
import * as ReactBootstapPagination from 'react-bootstrap/Pagination';
import PropTypes from 'prop-types';

class Pagination extends Component {
  constructor(props) {
    super(props);
    this.handlePageClick = this.handlePageClick.bind(this);
  }

  // trigger callback passing the current selected page
  handlePageClick(e) {
    this.props.handlePageClick(e.target.getAttribute('pageindex'));
  }

  // renders pagination items
  renderItems() {
    const { currentPage, pageTotal } = this.props;

    const items = [];
    for (let pages = 1; pages <= pageTotal; pages += 1) {
      items.push(
        <ReactBootstapPagination.Item
          key={pages}
          pageindex={pages}
          active={pages === currentPage}
          onClick={this.handlePageClick}
        >
          {pages}
        </ReactBootstapPagination.Item>,
      );
    }

    return items;
  }

  render() {
    return (
      <div className="cp-pagination">
        <ReactBootstapPagination>
          {this.renderItems()}
        </ReactBootstapPagination>
      </div>
    );
  }
}

Pagination.propTypes = {
  currentPage: PropTypes.number.isRequired,
  pageTotal: PropTypes.number.isRequired,
  handlePageClick: PropTypes.func.isRequired
};

export default Pagination;
