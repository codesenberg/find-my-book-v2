import React, { Component } from 'react';
import PropTypes from 'prop-types';
import StarRatingComponent from 'react-star-rating-component';
import noCoverSrc from '../../globals/images/no-cover.gif';
import './ResultItem.scss';


class ResultItem extends Component {
  render() {
    const {
      jacketCover, title, authors, averageRating, description
    } = this.props;

    return (
      <div className="cp-result-item">
        <div className="jacket-cover">
          <img width="128" height="206" src={jacketCover || noCoverSrc} alt="book cover" />
        </div>
        <div className="info">
          <span className="title">{title}</span>
          <br />
          <span className="author-label">By:</span>
          {authors && authors.join(', ')}
          {averageRating ? (
            <div>
              <StarRatingComponent
                name="book-rating"
                editing={false}
                starCount={5}
                value={4.5}
              />
            </div>
          ) : <div className="no-data"> No Rating</div>}
          {description ? <p className="description block-with-text">{description}</p> : <div className="no-data"> No description</div>}
        </div>
      </div>
    );
  }
}

ResultItem.propTypes = {
  title: PropTypes.string.isRequired,
  authors: PropTypes.arrayOf(PropTypes.string),
  description: PropTypes.string,
  averageRating: PropTypes.number,
  jacketCover: PropTypes.string
};

export default ResultItem;
