import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import FormControl from 'react-bootstrap/FormControl';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import ReactRouterPropTypes from 'react-router-prop-types';

import './SearchBar.scss';

// prevent page from reloading when user presses 'Enter' in search input field
function onSubmit(e) {
  e.preventDefault();
}

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.searchInputRef = React.createRef();
    this.handleSearch = this.handleSearch.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
  }

  onKeyDown(e) {
    // if user presses enter, trigger search
    if (e.keyCode === 13) {
      this.handleSearch();
    }
  }

  // redirect user to search result page with the search query appended to the url
  handleSearch() {
    this.props.history.push(`/search-results?query=${this.searchInputRef.current.value}`);
  }


  render() {
    return (
      <div className="cp-search-bar">
        <Form onSubmit={onSubmit} inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" ref={this.searchInputRef} onKeyDown={this.onKeyDown} />
          <Button variant="outline-success" onClick={this.handleSearch}>Search</Button>
        </Form>
      </div>
    );
  }
}

const RouterPropTypes = {
  history: ReactRouterPropTypes.history.isRequired
};

SearchBar.propTypes = {
  ...RouterPropTypes
};

export default withRouter(SearchBar);
