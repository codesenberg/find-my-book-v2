import React, { Component } from 'react';
import * as BootstrapNavBar from 'react-bootstrap/Navbar';
import './NavBar.scss';
import logo from '../../globals/images/logo.png';

class NavBar extends Component {
  render() {
    return (
      <div className="cp-nav-bar">
        <BootstrapNavBar bg="light" expand="lg">
          <BootstrapNavBar.Brand href="/" className="app-name">
            Find My Book
            <img src={logo} alt="logo" />
          </BootstrapNavBar.Brand>
        </BootstrapNavBar>
      </div>
    );
  }
}

export default NavBar;
